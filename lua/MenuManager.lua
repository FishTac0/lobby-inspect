if not _G.LobbyInspect then
	_G.LobbyInspect = _G.LobbyInspect or {}
	LobbyInspect.Name = "Lobby Inspect"
	LobbyInspect._path = ModPath
	LobbyInspect._data_path = SavePath .. "LobbyInspect.txt"
	LobbyInspect.settings =
	{
		mode = 2,
		rank = true,
		hours = true,
		skillpoints = true,
		achievements = true,
		vac = true,
		pd2cheats = true,
		steam_overlay = true,
		pd2stats_overlay = true,
		steamrep_overlay = true,
		friends = false,
		language = 1,
	}
	LobbyInspect.PlayerName = nil
	LobbyInspect.host_id = nil
	LobbyInspect.room_id = nil
	LobbyInspect.allow_connection = false
	LobbyInspect.Init = {false, false, false}
end

function LobbyInspect:InitStuff()
	dohttpreq("http://steamcommunity.com/",
	function(page)
		local _, start = string.find(page, '<span>View mobile website</span>')
		if start then
			LobbyInspect.Init[1] = true
		end
	end)

	dohttpreq("http://api.pd2stats.com/cheater/v3/?type=saf&id=",
	function(page)
		local _, start1 = string.find(page, 'id=')
		if start1 then
			LobbyInspect.Init[2] = true
		end
	end)

	dohttpreq("http://pd2stats.com/",
	function(page)
		local _, start2 = string.find(page, '<script type="text/javascript">')
		if start2 then
			LobbyInspect.Init[3] = true
		end
	end)
end
LobbyInspect:InitStuff()
function LobbyInspect:Load()
	local file = io.open(self._data_path, "r")
	if file then
		for k, v in pairs(json.decode(file:read("*all")) or {}) do
			self.settings[k] = v
		end
		file:close()
	end
end

function LobbyInspect:Save()
	local file = io.open(self._data_path, "w+")
	if file then
		file:write(json.encode(self.settings))
		file:close()
	end
end

Hooks:Add("LocalizationManagerPostInit", "LobbyInspect:LocalizationManager", function(loc)
	LobbyInspect:Load()
	local mpath = LobbyInspect._path  .. "loc/"
	loc:load_localization_file(mpath.."initial.json")
	local path
	if LobbyInspect.settings.language == 1 then
		path="english"
	elseif LobbyInspect.settings.language == 2 then
		path="french"
	elseif LobbyInspect.settings.language == 3 then
		path="german"
	elseif LobbyInspect.settings.language == 4 then
		path="italian"
	elseif LobbyInspect.settings.language == 5 then
		path="russian"
	elseif LobbyInspect.settings.language == 6 then
		path="turkish"
	elseif LobbyInspect.settings.language == 7 then
		path="spanish"
	end
	loc:load_localization_file(mpath..path .. ".json")
end)

Hooks:Add("MenuManagerInitialize", "LobbyInspect:MenuManagerInitialize", function(menu_manager)
	MenuCallbackHandler.lobby_inspect_type_callback = function(this, item)
		LobbyInspect.settings.mode = item:value()
	end
	MenuCallbackHandler.lobby_inspect_language_callback = function(this, item)
		LobbyInspect.settings.language = item:value()
	end
	MenuCallbackHandler.lobby_inspect_friends_callback = function(this, item)
		LobbyInspect.settings.friends = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_rank_callback = function(this, item)
		LobbyInspect.settings.rank = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_hours_callback = function(this, item)
		LobbyInspect.settings.hours = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_skillpoints_callback = function(this, item)
		LobbyInspect.settings.skillpoints = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_achievements_callback = function(this, item)
		LobbyInspect.settings.achievements = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_vac_callback = function(this, item)
		LobbyInspect.settings.vac = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_pd2cheats_callback = function(this, item)
		LobbyInspect.settings.pd2cheats = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_steam_overlay_callback = function(this, item)
		LobbyInspect.settings.steam_overlay = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_pd2stats_overlay_callback = function(this, item)
		LobbyInspect.settings.pd2stats_overlay = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_steamrep_overlay_callback = function(this, item)
		LobbyInspect.settings.steamrep_overlay = (item:value() == "on" and true or false)
	end
	MenuCallbackHandler.lobby_inspect_menu = function(this, item)
		LobbyInspect:Save()
	end
	LobbyInspect:Load()
	MenuHelper:LoadFromJsonFile(LobbyInspect._path .. "menu/options.json", LobbyInspect, LobbyInspect.settings)
end)

function ContinueConnecting()
	NetworkMatchMakingSTEAM:join_server(LobbyInspect.room_id)
	LobbyInspect.allow_connection = true
	DelayedCalls:Add("LobbyInspect:Disallow_connections", 0.2, function()
		LobbyInspect.allow_connection = false
	end)
end

function SteamFriend(user_id)
		local afriend = false
		if Steam:logged_on() then
			for _, friend in ipairs(Steam:friends() or {}) do
				if friend:id() == user_id then
					afriend = true
					break
				end
			end
		end
		return afriend
	end

function DataCollect()
	managers.menu:show_joining_lobby_dialog()
	DelayedCalls:Add("LobbyInspect:Force_close_stale", 10, function()
		if not Utils:IsInGameState() then
			managers.system_menu:close("join_server")
		end
	end)
	local hours = ""
	local pd2cheat = false
	local achievements = ""
	local rank = ""
	local vac = managers.localization:text("lobby_inspect_clean")
	local total = ""
	if LobbyInspect.Init[1] == false then
		hours = "down"
	end
	if LobbyInspect.Init[2] == false then
		pd2cheat = "down"
	end
	if LobbyInspect.Init[3] == false then
		rank = "down"
	end
	if LobbyInspect.Init[1] == true then
		dohttpreq("http://steamcommunity.com/profiles/".. LobbyInspect.host_id .. "/?l=english",
		function(page)
			if LobbyInspect.settings.hours == true then
				local _, hours_start = string.find(page, '<div class="game_info_details">')
				if hours_start then
					local hours_ends = string.find(page, '<div class="game_name"><a', hours_start)
					if hours_ends then
						hours = (string.sub(page, hours_start, hours_ends))
						hours = string.gsub(hours, "	", "")
						hours = string.gsub(hours, "hrs on record<br>", "")
						hours = string.gsub(hours, "<", "")
						hours = string.gsub(hours, ">", "")
						hours = string.split(hours, "\n")
						hours = hours[2]
						hours = string.gsub(hours, ",", "")
						hours = (math.floor((hours + 1/2)/1) * 1)
						hours = tonumber(hours)
						if hours ~= nil then
							hours = (math.floor((hours + 1/2)/1) * 1)
						end
					end
				end
			end
			if LobbyInspect.settings.vac == true then
				local _, vac_exists = string.find(page, '<div class="profile_ban_status">')
				if vac_exists then
					vac = managers.localization:text("lobby_inspect_warning")
				end
			end
			if hours ~= "" then
				if LobbyInspect.settings.achievements == true then
					local _, ach_start = string.find(page, '<h2>Recent Activity</h2>')
					if ach_start then
						local ach_ends = string.find(page, '<span>View mobile website</span>', ach_start)
						if ach_ends then
							local page1 = (string.sub(page, ach_start, ach_ends))
							if page1 then
								local _, ach1_start = string.find(page1, '<span class="ellipsis">')
								if ach1_start then
									local ach1_ends = string.find(page1, '<div class="achievement_progress_bar_ctn">', ach1_start)
									if ach1_ends then
										achievements = (string.sub(page1, ach1_start, ach1_ends))
										achievements = string.split(achievements, " of")
										local achievements2 = string.sub(achievements[2], 2, 4)
										achievements = achievements[1]
										achievements = string.gsub(achievements, ">", "")
										achievements = achievements .. " / " .. achievements2
										--log("achievements" .. achievements)
									end
								end
							end
						end
					end
				end
				if LobbyInspect.Init[2] == true then
					dohttpreq("http://api.pd2stats.com/cheater/v3/?type=saf&id=".. LobbyInspect.host_id .. "&force=1",
					function(page)
						if LobbyInspect.settings.pd2cheats == true then
							local Valid_reason = false
							local reason = ""
							for param, val in string.gmatch(page, "([%w_]+)=([%w_]+)") do
								if string.len(val) > 17 then
									reason = string.gsub(val, "_", " ")
									Valid_reason = string.find(reason, "Not enough heists completed") and true or false
									if Valid_reason == false then
										pd2cheat = true
										break
									end
								end
							end
						end
						if LobbyInspect.Init[3] == true then
							dohttpreq("http://pd2stats.com/stats.php?profiles=".. LobbyInspect.host_id .. "#skilldata",
							function(page)
								local _, rank_start = string.find(page, '<font color="#538eca">')
								if rank_start then
									local rank_ends = string.find(page, 'Steam Link', rank_start)
									if rank_ends then
										rank = (string.sub(page, rank_start, rank_ends))
										rank = string.split(rank, "\n")
										if rank[2] then
											rank = rank[1]
											rank = string.gsub(rank, ">", "")
											rank = string.gsub(rank, "]</font", "")
											rank = string.sub(rank, 2)
											rank = string.split(rank, "-")
											rank = "\nInfamy: " .. rank[1] .. "\nLevel: " .. rank[2]
										else
											rank = ""
										end
										--log("rank" .. rank)
									end
									local _, skills_start = string.find(page, '#1')
									if skills_start then
										local skills_ends = string.find(page, 'src="http://pd2stats.com/images/spade.png"/></font></a></li>', skills_start)
										if skills_ends then
											total = (string.sub(page, skills_start, skills_ends))
											total = string.gsub(total, " ", "")
											total = string.gsub(total, "<", " ")
											total = string.gsub(total, ">", " ")
											total = string.split(total, " ")
											total = total[3] .. " / " .. "120"
											--log("total" .. total)
										end
									end
								end
								PrepareData(hours, vac, achievements, pd2cheat, rank, total)
							end)
						else
							PrepareData(hours, vac, achievements, pd2cheat, rank, total)
						end
					end)
				else
					if LobbyInspect.Init[3] == true then
						dohttpreq("http://pd2stats.com/stats.php?profiles=".. LobbyInspect.host_id .. "#skilldata",
						function(page)
							local _, rank_start = string.find(page, '<font color="#538eca">')
							if rank_start then
								local rank_ends = string.find(page, 'Steam Link', rank_start)
								if rank_ends then
									rank = (string.sub(page, rank_start, rank_ends))
									rank = string.split(rank, "\n")
									if rank[2] then
										rank = rank[1]
										rank = string.gsub(rank, ">", "")
										rank = string.gsub(rank, "]</font", "")
										rank = string.sub(rank, 2)
										rank = string.split(rank, "-")
										rank = "\nInfamy: " .. rank[1] .. "\nLevel: " .. rank[2]
									else
										rank = ""
									end
									--log("rank" .. rank)
								end
								local _, skills_start = string.find(page, '#1')
								if skills_start then
									local skills_ends = string.find(page, 'src="http://pd2stats.com/images/spade.png"/></font></a></li>', skills_start)
									if skills_ends then
										total = (string.sub(page, skills_start, skills_ends))
										total = string.gsub(total, " ", "")
										total = string.gsub(total, "<", " ")
										total = string.gsub(total, ">", " ")
										total = string.split(total, " ")
										total = total[3] .. " / " .. "120"
										--log("total" .. total)
									end
								end
							end
							PrepareData(hours, vac, achievements, pd2cheat, rank, total)
						end)
					else
						PrepareData(hours, vac, achievements, pd2cheat, rank, total)
					end
				end
			else
				PrepareData(hours, vac, achievements, pd2cheat, rank, total)
			end
		end)
	else
		PrepareData(hours, vac, achievements, pd2cheat, rank, total)
	end
end

function PrepareData(hours, vac, achievements, pd2cheat, rank, total)
	if pd2cheat == true then
		pd2cheat =  "\n" .. managers.localization:text("lobby_inspect_pd2stats_status") .. ": " .. managers.localization:text("lobby_inspect_warning")
	elseif hours == "" then
		pd2cheat = ""
	elseif pd2cheat == "down" then
		pd2cheat = "\n" .. managers.localization:text("lobby_inspect_pd2statsanticheat_down") .. "!"
	else
		pd2cheat = "\n" .. managers.localization:text("lobby_inspect_pd2stats_status") .. ": " .. managers.localization:text("lobby_inspect_clean")
	end
	if hours == "" or rank == "down" then
		total = ""
	else
		total = "\n" .. managers.localization:text("lobby_inspect_skillpoints") .. ": " .. total
	end
	if hours == "" then
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. managers.localization:text("lobby_inspect_hidden")
	elseif hours == nil or hours == "nil" then
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. managers.localization:text("lobby_inspect_failed")
	elseif hours == "down" then
		hours = "\n" .. managers.localization:text("lobby_inspect_steam_down") .. "!"
		vac = ""
	else
		hours = "\n" .. managers.localization:text("lobby_inspect_playtime") .. ": " .. tostring(hours) ..  " " .. managers.localization:text("lobby_inspect_hours")
	end
	if achievements == "" then
		achievements = ""
	else
		achievements = "\n" .. managers.localization:text("lobby_inspect_achievements_title") .. ": " .. achievements
	end
	if rank == "down" then
		rank = "\n" .. managers.localization:text("lobby_inspect_pd2stats_down") .. "!"
		total = ""
	end
	local message = managers.localization:text("lobby_inspect_player") .. ": " .. LobbyInspect.PlayerName
	if NoobJoin then
		if NoobJoin:Is_From_Blacklist(LobbyInspect.host_id) == true then
			message = message .. "\n" .. managers.localization:text("lobby_inspect_is_from_blacklist")
		end
	end
	if LobbyInspect.settings.rank == true then
		message = message .. rank
	end
	if LobbyInspect.settings.hours == true then
		message = message .. hours
	end
	if LobbyInspect.settings.skillpoints == true then
		message = message .. total
	end
	if LobbyInspect.settings.achievements == true then
		message = message .. achievements
	end
	if LobbyInspect.settings.vac == true then
		message = message .. " \n" .. managers.localization:text("lobby_inspect_vac") .. ": " .. vac
	end
	if LobbyInspect.settings.pd2cheats == true then
		message = message .. pd2cheat
	end
	ContinueInspecting(message)
end

function DoHttpLookup(data)
	if data[1] == "steam" then
		Steam:overlay_activate("url", "http://steamcommunity.com/profiles/" .. LobbyInspect.host_id)
	elseif data[1] == "pd2stats" then
		Steam:overlay_activate("url", "http://pd2stats.com/profiles/" .. LobbyInspect.host_id)
	elseif data[1] == "steamrep" then
		Steam:overlay_activate("url", "http://steamrep.com/profiles/" .. LobbyInspect.host_id)
	end
	ContinueInspecting(data[2])
end

function NGBTO_Add_Cheater(data)
	local menu_options = {}
	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_ban"), data = nil, callback = NGBTO_Add_CheaterNow}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_cancel"), data = data[2], callback = ContinueInspecting}
	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), managers.localization:text("lobby_inspect_ban_confirm") .. " " .. LobbyInspect.PlayerName .. "?", menu_options)
	menu:Show()
end

function NGBTO_Add_CheaterNow()
	local menu_options = {}
	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_okay"), is_cancel_button = true}
	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), LobbyInspect.PlayerName .. " " .. managers.localization:text("lobby_inspect_banned") .. ".", menu_options)
	NoobJoin:Add_Cheater(LobbyInspect.host_id, LobbyInspect.PlayerName, "Manual ban")
	menu:Show()
end

function ContinueInspecting(message)
	local menu_options = {}
	local data_package = {{"steam", message},{"pd2stats", message},{"steamrep", message}}
	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_cancel"), is_cancel_button = true}
	menu_options[#menu_options+1] = {text = managers.localization:text("lobby_inspect_refresh"), data = nil, callback = DataCollect}
	if LobbyInspect.settings.steam_overlay == true then
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_steam_overlay_title"), data = data_package[1], callback = DoHttpLookup}
	end
	if LobbyInspect.settings.pd2stats_overlay == true then
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_pd2cheats_title"), data = data_package[2], callback = DoHttpLookup}
	end
	if LobbyInspect.settings.steamrep_overlay == true then
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_steamrep_overlay_title"), data = data_package[3], callback = DoHttpLookup}
	end
	if NoobJoin then -- If you have NGBTO you can ban people! For those lovely NGBTO users, shoutout to everyone in my friendlist, I love you guys. ;)
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_ban"), data = data_package[1], callback = NGBTO_Add_Cheater}
	end
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_connect"), data = nil, callback = ContinueConnecting}
	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), message, menu_options)
	managers.system_menu:close("join_server")
	menu:Show()
end

function Midstop()
	local menu_options = {}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_cancel"), is_cancel_button = true}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_inspect"), data = nil, callback = DataCollect}
	menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_connect"), data = nil, callback = ContinueConnecting}
	local message = managers.localization:text("lobby_inspect_question")
	if LobbyInspect.host_id == nil then
		message = managers.localization:text("lobby_inspect_error")
		menu_options = {}
		menu_options[#menu_options+1] ={text = managers.localization:text("lobby_inspect_cancel"), is_cancel_button = true}
	end
	local menu = QuickMenu:new(managers.localization:text("lobby_inspect_options"), message, menu_options, tweak_data.chat_colors[1])
	menu:Show()
end
