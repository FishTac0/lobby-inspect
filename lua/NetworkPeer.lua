Hooks:Add("NetworkManagerOnPeerAdded", "LobbyInspect:Sent_join_request", function(peer, peer_id)
	if peer_id == 1 and LobbyInspect and not Utils:IsInGameState() then
		if LobbyInspect.allow_connection == false then
			if SteamFriend(peer:user_id()) == true and LobbyInspect.settings.friends == false then
				return
			elseif LobbyInspect.settings.mode == 3 then
				return
			else
				LobbyInspect.PlayerName = peer:name()
				LobbyInspect.host_id = peer:user_id()
				managers.network.matchmake:leave_game()
				managers.network.voice_chat:destroy_voice()
				managers.network:queue_stop_network()
				managers.system_menu:close("waiting_for_server_response")
				if LobbyInspect.settings.mode == 1 then
					Midstop()
				elseif LobbyInspect.settings.mode == 2 then
					DataCollect()
				end
			end
		end
	end
end)