Hooks:PostHook(NetworkMatchMakingSTEAM, "join_server", "LobbyInspect:join_server", function(self, room_id, skip_showing_dialog)
	if LobbyInspect then
		LobbyInspect.room_id = room_id
	end
end)

local SearchLobbyOriginal = NetworkMatchMakingSTEAM.search_lobby
function NetworkMatchMakingSTEAM:search_lobby(friends_only)
	if LobbyInspect.allow_connection == true then
		return
	else
		SearchLobbyOriginal(self, friends_only)
	end
end