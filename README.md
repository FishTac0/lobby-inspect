### With this mod, you can look at the host's statistics like: hours, achievement count, infamy, level, VAC, Pd2stats lookup. ######
### And the best thing is that you will do all of this without even loading into the lobby. ######

---------------------------------------

Display information:
* Name
* Playtime
* Achievement count
* Infamy
* Level
* VAC status
* Pd2Stats status
* Total skill points count

Additional notes:

* Displays information about the host before connecting to the game.
* NGBTO integration, if you have it you can ban people straight from the inspection menu.
* Ability to go to hosts steam profile or pd2stats page.
* You can disable friends from being inspected.

![Logo](http://lastbullet.net/mydownloads/previews/preview_13030_1463602969_1f55ebeba69187426c039198a11dc254.png)
![Cheater](http://lastbullet.net/mydownloads/previews/preview_13030_1463518329_72463b6c8c82b3afe8760d94d14fe783.png)
![Hidden profile](http://lastbullet.net/mydownloads/previews/preview_13030_1463518331_db67c04b490dc78d39fbdaefa72d0c2f.png)
![Normal player](http://lastbullet.net/mydownloads/previews/preview_13030_1463518333_440fa8c81a37792a12e19cfb2b62ed81.png)
![Total skill point cheater](http://lastbullet.net/mydownloads/previews/preview_13030_1463665299_e02f887e9cb50961746a56e4a8737e14.jpg)
